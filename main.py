#!/usr/bin/python

from pad4pi import rpi_gpio
import json
import logging
import threading
import time
import soundplayer
from threading import Timer
from pygame import mixer, time as pyg_time

# Read config in
conf = json.load(open('conf/settings.json'))
print(conf)

KEYPAD = [
    ["1", "2", "3"],
    ["4", "5", "6"],
    ["7", "8", "9"],
    ["*", "0", "#"]
]

# On the keypad I have, with the pad facing towards you:
# |COL1|COL2|COL3|ROW1|ROW2|ROW3|ROW4|NUL|
COL_PINS = conf['COL_PINS']
ROW_PINS = conf['ROW_PINS']

# Plays a beep noise when a key is pressed
def beep():
    soundplayer.play_sound("sounds/beep.wav")

# Holds the numbers pressed so far
num_pressed_buffer = []

def play_noise():
    global num_pressed_buffer
    dialed_num = ""

    for num in num_pressed_buffer:
        dialed_num += num

    # Clear out the buffer
    num_pressed_buffer = []
    try:
        sound_path = conf['FILE_MAP'][str(dialed_num)]
        print("Playing Noise " + sound_path)
        soundplayer.play_sound(sound_path)
    except KeyError:
        return

def print_key(key):
    threading.Thread(target=beep).start()
    print("Key Pressed: " + key)
    global press_timer
    press_timer.cancel()
    press_timer = Timer(2, play_noise) # extract to func
    press_timer.start()
    num_pressed_buffer.append(key)

press_timer = Timer(2, play_noise)

try:
    factory = rpi_gpio.KeypadFactory()

    keypad = factory.create_keypad(keypad=KEYPAD, row_pins=ROW_PINS, col_pins=COL_PINS)

    keypad.registerKeyPressHandler(print_key)

    print("Press buttons on your keypad. Ctrl+C to exit.")
    while True:
        pass

except KeyboardInterrupt:
    keypad.cleanup()
    print("Goodbye")
finally:
    keypad.cleanup()
