from pygame import error as pyg_err, mixer, time as pyg_time
import time

def play_sound(sound_path):
    try:
        mixer.init()
        noise = mixer.Sound(sound_path)
        clock = pyg_time.Clock()
        noise.play()
        time.sleep(noise.get_length() + 1)
        mixer.quit()
    except pyg_err:
        print("Sound file not found.")
        return
